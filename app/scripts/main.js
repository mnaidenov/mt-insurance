(function ($, window) {
  'use strict';

  $(window).on('load', function onWinLoad(){
    MetriksFactory('init');
  });

  $('.calc__wrap').on('click mousedown touchstart touchmove', function () {
    $('.rangeslider').removeClass('active');
    $(this).addClass('active');
  });

  var $insuranceSliderInputs = $('.rangeslider input');
  $insuranceSliderInputs.ionRangeSlider({
    type: 'single',
    keyboard: true,
    grid: false,
    onStart: function (data) {
      // $('#insurance-1.rangeslider').addClass('active');
    },
    onChange: function (data) {
      var $thisElement = data.slider,
        $parent = $thisElement.parents('.calc__wrap'),
        parentID = $parent.attr('id'),
        $amount = $parent.find('.wrap__info__amount').children('.total'),
        $insurance1 = $('#insurance-1'),
        $insurance2 = $('#insurance-2'),
        value = data.from;

      var amount1, amount2, amount3,
        annualInsurance = 0, totalInsurance = 0, month3Insurance = 0;

      if (parentID === $insurance1[0].id) {
        month3Insurance = (value * 0.25 / 100) / 4;
        totalInsurance = value;

        function round5 (x) {
          return Math.floor(x / 5) * 5;
        }

        var roundValue = round5(month3Insurance);

        $insurance2.find('input').data('ionRangeSlider').update({
          to: roundValue,
          from: roundValue
        });

        $insurance2.find('.total').text(addSpace(roundValue.toFixed(2)));

      }

      if (parentID === $insurance2[0].id) {
        annualInsurance = value * 4;
        totalInsurance = (annualInsurance / 0.25) * 100;

        $insurance1.find('input').data('ionRangeSlider').update({
          to: totalInsurance.toFixed(2),
          from: totalInsurance.toFixed(2)
        });
        $insurance1.find('.total').text(addSpace(totalInsurance.toFixed(2)));
        ;
      }

      $amount.text(addSpace(value + ',00'));

      // Calculate results
      amount1 = (totalInsurance * 0.75).toFixed(2);
      amount2 = (totalInsurance * 0.15).toFixed(2);
      amount3 = (totalInsurance * 0.10).toFixed(2);

      $('#amount-1 .amount').text(addSpace(amount1));
      $('#amount-2 .amount').text(addSpace(amount2));
      $('#amount-3 .amount').text(addSpace(amount3));
    },
    onFinish: function (data) {
      this.onChange(data);
    },
    onUpdate: function (data) {
    }
  });

  function addSpace (nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;

    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ' ' + '$2');
    }
    return x1 + x2;
  }

  var FORM_MSG = {
    FORM_SEND_SUCCESS: 'Ваша заявка успешно отправлена! В ближайшее время мы с вами<br/>свяжемся для заключения договора!',
    FORM_SEND_ERROR: 'По какой-то причине ваша заявка не была отправлена. Произошла ошибка.'
  };


  // validate form
  var $form = $('.form');
  if ($form.length < 1) return;
  parsley();
  var $formParsley = $form.parsley();
  var $formSuccess = $('.form-success');
  var $formError = $('.form-error');


  $('.form [type=\'submit\']').on('click', function (e) {
    $formParsley.validate();
    e.preventDefault();
  });

  $formParsley.on('form:validated', function () {
    var ok = $('.error').length === 0;
    if (ok) {
      sendForm(function (err, msg) {
        if (err) return formChangeStateTo('error', err.message);
        window.location = $form.data('redirect-to') || '/thank-you';
      });
    }
  });

  function sendForm (cb) {
    var formData = $form.serialize();
    var rqMethod = $form.attr('method');
    var rqUrl = $form.attr('action');
    var additionalParams = $.param({sb: MetriksFactory()});

    if(additionalParams){
      formData = [formData, additionalParams].join('&');
    }

    $.ajax(rqUrl, {method: rqMethod, data: formData})
      .done(function (data, statusText) {
        //on success
        if (data.error) cb(new Error(data.message));
        else cb(null, data.message);
      })
      .fail(function (_, statusText) {
        //on error
        console.log(statusText);
        cb(new Error('Не удалось отправить запрос.'));
      });
  }

  function formChangeStateTo (stateString) {
    var args = [].slice.call(arguments, 1);
    switch (stateString) {
      case true:
        formClearState();
        break;
      case 'success':
        formShowSuccess.apply(null, args);
        deferredFromClearState(null, true);
        break;
      case 'error':
        formShowError.apply(null, args);
        deferredFromClearState();
        break;
    }
  }

  function formClearState (isResetNeed) {
    $form.show();
    if (isResetNeed) $form.trigger('reset');
    $formError.hide();
    $formSuccess.hide();
  }

  function formShowSuccess (message) {
    $form.hide();
    $formSuccess.show();
    $formSuccess.find('p').text(message || FORM_MSG.FORM_SEND_SUCCESS);
  }

  function formShowError (message) {
    $form.hide();
    $formSuccess.show();
    $formSuccess.find('p').text(message || FORM_MSG.FORM_SEND_ERROR);
  }

  function deferredFromClearState (time, isResetNeed) {
    setTimeout(function () {
      formClearState(isResetNeed);
    }, time || 2000);
  }

  function parsley () {
    $('#form').parsley({
      successClass: 'success',
      errorClass: 'error',
      classHandler: function (el) {
        return el.$element.parents('label');
      },
      errorsWrapper: '',
    })
  }

  //masks
  $('input[data-type="phone"]').inputmask({'mask': '+375-99-999-99-99'});


  // tooltip
  $('[data-toggle="tooltip"]').tooltip();

  $('input[data-type="letters"]').on('input', function (event) {
    var inputText = $(this).val();

    var resultText = inputText.replace(/[^а-яё]/gi, '');
    $(this).val(resultText);
  });

  // simple scrollTo
  $('a[href*="#"]:not([href="#"])').on('click', function (e) {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });


  function MetriksFactory (method) {
    if ('sbjs' in window) {
      var sbjs = window.sbjs;
      if(method && method in sbjs && 'function' == typeof sbjs[method]){
        var methodFn = sbjs[method];
        return methodFn.call(sbjs);
      }
      return window.sbjs.get;
    }
    return {};
  }


})(jQuery, window);
